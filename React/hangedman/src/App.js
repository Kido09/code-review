import React, { Component } from 'react'
import './App.css'
import Alphabet from './Alphabet'
import GuessWord from './GuessWord'
import GuessCount from './GuessCount'

class App extends Component {
  state = {
    alphabet: this.generateAlphabet(),
    feedback: [],
    guessword: this.generateGuessWord(),
    usedLetters: [],
    guesses: 0,
  }
  unique_char(str1){
    var str=str1;
    var uniqchar="";
    for (let i = 0; i < str.length; i++) {
      if (uniqchar.indexOf(str.charAt(i)) === -1) {
        uniqchar += str[i];  
      }
    }
  return uniqchar;  
}
  generateGuessWord(){
    var words = ['COUCOU JE SUIS BEAU', 'MARCH'];
    var wordToFind = words[Math.floor(Math.random() * words.length)];
    return wordToFind.split('')
  }
  getFeedback = (index, letter) => {
    const {feedback, guessword, usedLetters, guesses} = this.state;
    const newguess = guesses + 1
    if ((guessword.join('').indexOf(letter)) >= 0) {
      feedback[index] = 'justMatched';
      usedLetters[index] = letter
      this.setState({feedback: feedback, usedLetters: usedLetters})
    }
    else if (guessword.join('').indexOf(letter) === -1) {
      feedback[index] = 'justMismatched';
      this.setState({feedback: feedback})
    }
    this.setState({guesses : newguess})
  }
  generateAlphabet() {
    var i = 0
    var alphabet = []
    while(i < 26) {
      alphabet.push(String.fromCharCode(i + 65))
      i++
    }
    return alphabet
  }
  reset = () => {
    var resetGuess = 0
    var resetFeedback = []
    var resetAlphabet = this.generateAlphabet()
    var resetGuessWord = this.generateGuessWord()
    var resetUsedLetters = []
    this.setState({feedback: resetFeedback, usedLetters: resetUsedLetters, alphabet: resetAlphabet, guesses: resetGuess, guessword: resetGuessWord})
  }
  render() {
    const {guessword, alphabet, feedback, usedLetters, guesses} = this.state
    const won = this.unique_char(guessword.join('').replace(/\s/g,'')).length === usedLetters.join('').length
    return (
      <div className="hangedman">
        <div className="guesscountWrap">
          <GuessCount guesses={guesses}/>
          {guessword.map((guessword, index) => (
            <GuessWord
              key={index}
              guessword={guessword}
              usedLetters={usedLetters}
            />
          ))}
        </div>
        <div className="alphabetWrap">
          {!won && alphabet.map((alphabet, index) => (
            <Alphabet
              key={index}
              alphabet={alphabet}
              onClick={this.getFeedback}
              feedback={feedback}
              index={index}
            />
          ))}
        <div className="winWrap">
          {won && <h1>Gagné en {guesses} essais ! {guesses === usedLetters.join('').length ? 'Victoire Parfaite' : ''}</h1>}
          {won && <button onClick={this.reset}>Rejouer ?</button>}
        </div>
        </div>
      </div>
    )
  }
}

export default App

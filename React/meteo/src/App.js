import React, { Component } from 'react'
import './App.css'
import MeteoCards from './components/MeteoCards'
import Title from './components/Title';
import SearchMeteo from './components/SearchMeteo'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

const METEO_API_KEY = "88c82e17e4dcb08b521d6114d7eca000"

export default class App extends Component {
  state = {
    temperature: undefined,
    city: undefined,
    country: undefined,
    humidity: undefined,
    description: undefined,
    error: undefined,
  }
  fillCardWithApi = async (e) => {
    e.preventDefault();
    const city = e.target.elements.Ville.value;
    const country = e.target.elements.Pays.value;
    const call_api = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&units=metric&lang=fr&appid=${METEO_API_KEY}`);
    const data = await call_api.json();
    console.log(data);
    if (city && country && (data.cod === 200)) {
      this.setState({
        temperature: data.main.temp,
        city: data.name,
        country: data.sys.country,
        humidity: data.main.humidity,
        description: data.weather[0].description,
        error: "",
      })
    }
    else {
      this.setState({
        temperature: undefined,
        city: undefined,
        country: undefined,
        humidity: undefined,
        description: undefined,
        error: "Please enter a valid value",
      })
    }
  }
  render() {
    return (
      <div>
        <div className="wrapper">
          <div className="main">
              <Row>
                <Col xs={5} className="title-container">
                  <Title />
                </Col>
                <Col xs={7} className="form-container">
                  <SearchMeteo fillCardWithApi={this.fillCardWithApi} />
                  <MeteoCards
                    temperature={this.state.temperature} 
                    humidity={this.state.humidity}
                    city={this.state.city}
                    country={this.state.country}
                    description={this.state.description}
                    error={this.state.error}
                  />
                </Col>
              </Row>
          </div>
        </div>
      </div>
    );
  }
};

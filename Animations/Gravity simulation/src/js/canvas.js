import utils from './utils'

const canvas = document.querySelector('canvas')
const c = canvas.getContext('2d')

canvas.width = innerWidth
canvas.height = innerHeight

const mouse = {
    x: innerWidth / 2,
    y: innerHeight / 2
}

const colors = ['#05668D', '#028090', '#00A896', '#02C39A', '#F0F3BD']

var gravity = 0.2;
var friction = 0.80;

function randomIntFromRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

function randomColor(colors) {
    return colors[Math.floor(Math.random() * colors.length)]
  }

// Event Listeners
addEventListener('mousemove', event => {
    mouse.x = event.clientX
    mouse.y = event.clientY
})

addEventListener('resize', () => {
    canvas.width = innerWidth
    canvas.height = innerHeight

    init()
})

addEventListener('click', () => {
    init();
})

// Objects
function Ball(x, y, dx, dy, radius, color) {
    this.x = x
    this.y = y
    this.dx = dx
    this.dy = dy
    this.radius = radius
    this.color = color
}

Object.prototype.draw = function() {
    c.beginPath();
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    c.fillStyle = this.color;
    c.fill();
    c.strokeStyle = '#02C39A'
    c.stroke();
    c.closePath();
}

Object.prototype.update = function() {
    if (this.y + this.radius + this.dy > canvas.height) {
        this.dy = -this.dy * friction;
    }
    else {
        this.dy += gravity;
    }

    if (this.x + this.radius + this.dx > canvas.width || this.x - this.radius < 0) {
        this.dx = -this.dx;
    }
    this.x += this.dx
    this.y += this.dy
    this.draw()
}

// Implementation
var ballArray
function init() {
    ballArray = []
    for (let i = 0; i < 500; i++) {
        var x = randomIntFromRange(radius, canvas.width - radius);
        var y = randomIntFromRange(0, canvas.height - radius);
        var dx = randomIntFromRange(-2, 2);
        var dy = randomIntFromRange(-2, 2);
        var radius = randomIntFromRange(12, 20); 
        var color = randomColor(colors);
        ballArray.push(new Ball(x, y, dx, dy, radius, color));
    }
}

// Animation Loop
function animate() {
    requestAnimationFrame(animate)
    c.clearRect(0, 0, canvas.width, canvas.height)

     ballArray.forEach(ball => {
      ball.update()
     })
}

init()
animate()

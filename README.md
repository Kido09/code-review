# Intro

Ceci est une collection de projets variés, le but étant de montrer les differentes notions que j'ai pue aborder.

## Dossiers

```bash
  -- Animations  
```
Des animations faites sur canvas

```bash
  -- React
```
Deux exercices fait sur React afin de découvrir le language

```bash
  -- Vue.js  
```
Une landing Page Vue.js, partant d'un template de [CreativeTim](https://www.creative-tim.com/product/vue-material-kit).

## Wordpress

Voici une liste non exhaustive de sites que j'ai crée sur Wordpress à l'aide du thème MF Theme.
Quelques plugins utilisés : Essential Grid, WooCommerce, Slider-révolution, WPBakery

http://absarchi.inum-web.online
http://atelierjfa.inum-web.online
http://gerard-paquet.inum-web.online
http://bois2bout.inum-web.online


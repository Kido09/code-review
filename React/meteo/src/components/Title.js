import React, { Component } from 'react'

export default class Title extends Component {
    render() {
        return (
            <div>
                <h1 className="title-container__title">MétéReact</h1>
                <p className="title-container__subtitle">Trouvez la météo près de chez vous</p>
            </div>
        )
    }
}

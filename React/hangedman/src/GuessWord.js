import React from 'react'
import './GuessWord.css'

const GuessWord = ({guessword, usedLetters}) => (
    <div className="word" >
      <span>
        {usedLetters.indexOf(guessword) !== -1 ? guessword : (guessword === ' ' ? ' ' : '_')}
      </span>
    </div>
  )

export default GuessWord
import React from 'react'
import './Alphabet.css'

const Alphabet = ({alphabet, onClick, feedback, index}) => (
    <div className={`alphabet ${feedback[index]}`} onClick={() => onClick(index, alphabet)} >
      <span>
        {alphabet}
      </span>
    </div>
  )

export default Alphabet
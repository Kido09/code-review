import React from 'react'

import './GuessCount.css'

const GuessCount = ({guesses}) =>    <div className="guesses">Tries {guesses}</div>

export default GuessCount

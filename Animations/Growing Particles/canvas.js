var canvas = document.getElementById('grow-particle')
canvas.height = window.innerHeight
canvas.width = window.innerWidth

var c = canvas.getContext('2d');
//rectangles
/*
c.fillStyle = 'rgba(255, 0, 0, 0.5)';
c.fillRect(100, 100, 100, 100);
//lines
c.beginPath();
c.moveTo(300, 400);
c.lineTo(450, 50);
c.stroke();
// arc / circle
/*c.beginPath();
c.arc(300, 300, 30, 0, Math.PI * 2, false);
c.strokeStyle = 'blue';
c.stroke();


for (let i = 0; i < 5000; i++) {
    var x = Math.random() * window.innerWidth;
    var y = Math.random() * window.innerHeight;
    c.beginPath();
    c.arc(x, y, 30, 0, Math.PI * 2, false);
    c.strokeStyle = '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
    c.stroke();
}*/
    var mouse = {
        x: undefined,
        y: undefined
    }

    var maxRadius = 40;
    var minRadius = 2;

    var colorArray = [
        '#05668D',
        '#028090',
        '#00A896',
        '#02C39A',
        '#F0F3BD'
    ]

    window.addEventListener('mousemove',
    function(event) {
        mouse.x = event.x;
        mouse.y = event.y;
    })

    window.addEventListener('resize',
    function() {
        canvas.height = window.innerHeight
        canvas.width = window.innerWidth

        init();
    })

    function Circle(x, y, dx, dy, radius, color) {
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
        this.radius = radius;
        this.minRadius = radius;
        this.color = colorArray[Math.floor(Math.random() * colorArray.length)];

        this.drawCircle = function() {
            c.beginPath();
            c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
            c.fillStyle = this.color;
            c.fill();
        }
        /*this.drawPenta = function() {
            c.beginPath();
 
            for ( var i = 0; i <= 4 * Math.PI; i += ( 4 * Math.PI ) / 5 ) {
                c.lineTo( this.x + this.radius * Math.cos(i + 1), this.y + this.radius * Math.sin(i + 1));
            }
 
            if ( false ) {
                c.moveTo( this.x + this.radius, this.y );
                c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
            }
            c.strokeStyle = 'red';
            c.stroke();
        }*/
        this.update = function() {
            if (this.x + this.radius > innerWidth || this.x - this.radius < 0) {
                this.dx = -this.dx;
            }
            if (this.y + this.radius > innerHeight || this.y - this.radius < 0) {
                this.dy = -this.dy;
            }
            this.x += this.dx;
            this.y += this.dy;

            //interractivity
            if (mouse.x - this.x < 50 && mouse.x - this.x > -50 && mouse.y - this.y < 50 && mouse.y - this.y > -50) {
                if (this.radius < maxRadius) {
                    this.radius += 4;
                }
            }
            else if (this.radius > this.minRadius) {
                this.radius -= 1;
            }

            this.drawCircle()
        }
    }

    var circleArray = [];

    function init() {

        circleArray = [];

        for(let i = 0; i < 1500; i++) {
            var x = Math.random() * (innerWidth - radius * 2) + radius;
            var y = Math.random() * (innerHeight - radius * 2) + radius;
            var dx = (Math.random() - 0.5);
            var dy = (Math.random() - 0.5);
            var radius = (Math.random() * 3) + 1;
            var color = '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
            circleArray.push(new Circle(x, y, dx, dy, radius, color))
        }
    }

    function animate() {

        c.clearRect(0, 0, innerWidth, innerHeight)
        for (let i = 0; i < circleArray.length; i++) {
            circleArray[i].update()
        }
        requestAnimationFrame(animate);
    }
    requestAnimationFrame(animate);

    init();